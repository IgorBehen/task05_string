package com.ihor;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class App {

    void countWords(String input) {
        String regex = "\\b(\\w+)(\\s+\\1\\b)+";
        int count = 0;
        // Use compile(regex) if you want case sensitive.
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(input);
        while (m.find()) {
            input = input.replaceAll(m.group(), m.group(1));
            count++;
        }
        System.out.println("Input: " + input + " Count of duplicate words are - " + count + ".\n");
    }

    void countWords1(String input) {////??????????????? не зрозуміло
        String pattern = "\\b(\\w+)\\b[\\w\\W]*\\b\\1\\b";
        Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        //String phrase = "unique is not duplicate but unique, Duplicate is duplicate.";
        Matcher m = p.matcher(input);
        while (m.find()) {
            String val = m.group();
            System.out.println("Matching subsequence is: \"" + val + "\"");
            System.out.println("Duplicate word: " + m.group(1) + "\n");
        }
    }

    void uniqueWords(String input) {
        //String[] words = input.split("[!-~]* ");
        String[] words = input.split("[-!~\\s]+");
        Set<String> uniqueWords = new HashSet<String>();
        System.out.print("Unique Words: ");

        for (String word : words) {
            uniqueWords.add(word);
        }
        for (String word1 : uniqueWords) {
            System.out.print(word1 + ", ");
        }
        System.out.println("\n");
    }


    //    public static List<String>  lookup( Map<String, String> map) {
//        String pattern = "\\b(\\w+)\\b[\\w\\W]*\\b\\1\\b";
//        final Pattern p = Pattern.compile(pattern);
//        List<String> values  = map.keySet()
//                .stream()
//                .filter(string -> p.matcher(string).matches())
//                .collect(Collectors.toList());
//        if(values!= null && !values.isEmpty()){
//            return values.stream().map((key) -> map.get(key)).collect(Collectors.toList());
//
//        }
//
//        return values;
//    }


    void findSentencesWithQuestionMark(String input) {
        List<String> mg = new ArrayList<>();
        Pattern pattern = Pattern.compile("[^.!?]+\\?");
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            mg.add(matcher.group());
            // System.out.print(matcher.group() + " / ");
        }
//        for (String s : mg) {
//            System.out.print(s + " / ");
//        }
        String st = "";// ArrayList<>(); to String
        for (String s : mg) {
            st += s + "\t";
        }
        System.out.println("st: " + st);
        //---- знаходить задану кількість слів??????
        Pattern pattern1 = Pattern.compile("[a-zA-Z._ ^%$#!~@,-{}]+");
        Matcher matcher1 = pattern1.matcher(st);
        System.out.print("Sentences With Question Mark: ");
        while (matcher1.find()) {
            //matcher1.group();
            System.out.print(matcher1.group() + "/");
        }
        System.out.println("\n");
    }


    void ifTextContainsCertainWordCaseSensitive(String input, String searchingWord) { //for searching certain word. !!!case sensitive!!!(return -> true or false )
        String pattern = ".*" + searchingWord + ".*";
        boolean isMatch = Pattern.matches(pattern, input);
        System.out.println("Searching certain word - Case Sensitive...   The text contains '" + searchingWord + "'? " + isMatch);
    }

    void ifTextContainsCertainWordCaseINsensitive(String input, String searchingWord) { //for searching certain word. !!!case INsensitive!!!(return -> true or false )
        String patternString = ".*" + searchingWord + ".*";
        Pattern pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);
        boolean isMatched = matcher.matches();
        System.out.println("Searching certain word - Case Insensitive...   The text contains '" + searchingWord + "'? " + isMatched);// System.out.println("Is it a Match?" + isMatched);
    }

    void findWordsWithCertainSequenceOfLetters() {// Для чого???
        // returns true if the string contains of three letters
        System.out.println(
                Pattern.matches("[a-zA-Z][a-zA-Z][a-zA-Z]", "aPz"));//True //mathing every leter to every block of letters
    }

    void findWordsWithCertainSequenceOfLettersMultipleOccurrences(String input, String searchingSequence) { //скільки разів і з якої літкри по яку зустрічається послідовність літер
        Pattern pattern = Pattern.compile(searchingSequence);
        Matcher matcher = pattern.matcher(input);
        while (matcher.find()) {
            System.out.println("Found at: " + matcher.start()
                    +
                    " - " + matcher.end());
        }
        System.out.println();
    }


    void findSpecificWord(String input, String specificWord) { //with idexes
        //String data1 = "Today, java is object oriented language";
        String regex = "\\b" + specificWord + "\\b";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);
        while (matcher.find()) {
            System.out.print("Start index: " + matcher.start());
            System.out.print(" End index: " + matcher.end() + " ");
            System.out.println(matcher.group());
        }
        System.out.println();
    }

    void findSpecificSubsequenceInWords(String input, String specificWord) {
        String regex = "\\B" + specificWord + "|" + specificWord + "\\B";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);
        while (matcher.find()) {
            System.out.print("Start index: " + matcher.start());
            System.out.print(" End index: " + matcher.end() + " ");
            System.out.println(matcher.group());
        }
    }

    void totalNumberOfWords(String input) {
        String[] arr = input.split(" ");
        int word = 0;
        for (int i = 0; i < arr.length; i++) {
            word++;
        }
        System.out.println("\nTotal Number Of Words is: " + word);
    }

    void arrangeWordsInDescendingOrderOfTheirLength(String input) {
        System.out.print("\nWords In Descending Order Of Their Lengt: ");
        for (int i = input.length(); i > 0; i--) {
            Matcher m = Pattern.compile("\\b\\w{" + i + "}\\b").matcher(input);
            while (m.find()) {
                System.out.print(m.group(0) + " ");
            }
        }
    }


    void removeDuplicatedWords(String input) {
        String regex = "\\b(\\w+)(\\s+\\1\\b)+";
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(input);
        System.out.print("\n\nWithout Removed Duplicated Words: ");
        while (m.find()) {
            input = input.replaceAll(m.group(), m.group(1));
        }
        System.out.println(input);
    }

    void findAndReplaceText(String input, String regexStr, String replacementStr) {
        Pattern pattern = Pattern.compile(regexStr, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);
        String outputStr = matcher.replaceAll(replacementStr);     // all matches
        //String outputStr = matcher.replaceFirst(replacementStr);     // first match only
        System.out.println("\n\nReplaced Text: " + outputStr);
    }

//    void removesWhitespaceBetweenAWords(String input){// Removes whitespace between a word character and . or ,
//    String pattern = "(\\w)(\\s+)([\\.,])";
//        System.out.println(" removesWhitespaceBetweenAWords");
//    System.out.println(input.replaceAll(pattern, "$1$3"));
//    }

    void findDuplicatedWords(String input) { //find all duplicated words
        Pattern pattern = Pattern.compile("\\b(\\w+)\\s+\\1\\b");
        Matcher matcher = pattern.matcher(input);
        System.out.print("\nFind all duplicated words: ");
        while (matcher.find()) {
            System.out.print(matcher.group() + "/");
        }
        System.out.println();
    }

    void separationBySentences(String s) { //separation by sentences (each new line)
        System.out.println("Separation by sentences (each new line): ");
        do {
            System.out.println(s.substring(0, (s.indexOf("?") + 1)));
            System.out.println(s.substring(0, (s.indexOf(".") + 1)));
            s = s.substring((s.indexOf("?") + 1), s.length());
            s = s.substring((s.indexOf(".") + 1), s.length());

            s = s.trim();

        } while ((s.indexOf(".") != -1) || (s.indexOf("?") != -1));
        System.out.println();
    }

    public void /*String[]*/ extractFirstSentences(String input) {
        String[] paragraphs = input.split("\\n");
        List<String> result = new ArrayList<String>();
        for (String paragraph : paragraphs) {
            result.add(paragraph.split("[\\.\\?\\!][\\r\\n\\t ]+")[0] + ".");
            System.out.println(result);
        }
        System.out.println();
    }

    public void sortWordsAlphabetically(String s) {
        Pattern PATTERN = Pattern.compile("\\s+");
        System.out.println(PATTERN.splitAsStream(s).sorted().collect(Collectors.joining(" ")));
        System.out.println();
    }


//    void startWithVowel(String word) {
//        System.out.println("eaiouEAIOU".indexOf(word.charAt(0)) >=0);
//    }


//    void printWordsStartingVowel(String input) { // Робоче, але не то, великий код і т.д.
//        String s = input;
//        String w;
//        int pos = 0;
//        char ch1, ch2;
//        int l = s.length();
//        s = s + " ";
//        for (int i = 0; i < l; i++) {
//            ch1 = s.charAt(i);
//            if (ch1 == ' ') {
//                w = s.substring(pos, i); // extracting words one by one
//                ch2 = w.charAt(0);
//                if (ch2 == 'A' || ch2 == 'E' || ch2 == 'I' || ch2 == 'O' || ch2 == 'U' ||
//                        ch2 == 'a' || ch2 == 'e' || ch2 == 'i' || ch2 == 'o' || ch2 == 'u') {
//                    System.out.println(w);
//                }
//                pos = i + 1;
//            }
//        }
//    }

//    void numberVowelsAndConsonants(String input) { // Робоче, але не то, великий код і т.д.
//        int vowels = 0, consonants = 0;
//        String line = input.toLowerCase();
//        for (int i = 0; i < line.length(); ++i) {
//            char ch = line.charAt(i);
//            if (ch == 'a' || ch == 'e' || ch == 'i'
//                    || ch == 'o' || ch == 'u') {
//                ++vowels;
//            } else if ((ch >= 'a' && ch <= 'z')) {
//                ++consonants;
//            }
//        }
//        System.out.println("\nVowels: " + vowels);
//        System.out.println("Consonants: " + consonants);
//    }

    void wordsStartingVowel(String input) {
        System.out.print("\nWords that starting by a vowel: \n");
        String[] wordsArr = input.split(" ");
        List<String> words = Arrays.asList(wordsArr);
        Pattern p = Pattern.compile("^[aeiou].*", Pattern.CASE_INSENSITIVE);
        List<String> startingWithVowels =
                words.stream().filter(w -> p.matcher(w).matches()).collect(Collectors.toList());
        for (String startingWithVowel : startingWithVowels) {
            System.out.println(startingWithVowel);
        }
    }

    void wordsStartingConsonant(String input) {
        System.out.print("\nWords that starting by a consonant: \n");
        String[] wordsArr = input.split(" ");
        List<String> words = Arrays.asList(wordsArr);
        Pattern p = Pattern.compile("^[bcdfghjklmnpqrstvxzwy].*", Pattern.CASE_INSENSITIVE);
        List<String> startingWithConsonants =
                words.stream().filter(w -> p.matcher(w).matches()).collect(Collectors.toList());
        for (String startingWithConsonant : startingWithConsonants) {
            System.out.println(startingWithConsonant);
        }
    }

    public static void main(String[] args) {
        App app = new App();
        //String  st = "Count how many words Map each word plt the string by space i.e., word";
        String input = "The the indef indef ghistringdddd String 1string stringing? " +
                "Son agreed others exeter period myself few yet nature. " +
                "Mention mr manners opinion if garrets enabled. " +
                "\n\r To an occasional dissimilar book book impossible sentiments? " +
                "Do fortune account written prepare invited no passage. " +
                "Garrets use ten you the weather agreed ferrars venture friends. " +
                "\n\r Solid visit seems again you nor all. " +
                "Count1 how many words? " +
                "The the string String abc string string ing.";
        app.countWords(input);
        app.countWords1(input);
        app.uniqueWords(input);
        app.findSentencesWithQuestionMark(input);
        app.ifTextContainsCertainWordCaseSensitive(input, "ferrars");
        app.ifTextContainsCertainWordCaseINsensitive(input, "Ferrars");
        app.findWordsWithCertainSequenceOfLetters();
        app.findWordsWithCertainSequenceOfLettersMultipleOccurrences(input, "gree");
        app.findSpecificWord(input, "seems");
        app.findSpecificSubsequenceInWords(input, "string");
        app.totalNumberOfWords(input);
        app.arrangeWordsInDescendingOrderOfTheirLength(input);
        app.removeDuplicatedWords(input);
        app.findAndReplaceText(input, "string", "HELLO");
        //app.removesWhitespaceBetweenAWords(input);
        app.findDuplicatedWords(input);
        app.separationBySentences(input);
        app.extractFirstSentences(input);
        app.sortWordsAlphabetically(input);
        //app.startWithVowel(input);
        //app.printWordsStartingVowel(input);
        //app.numberVowelsAndConsonants(input);
        app.wordsStartingVowel(input);
        app.wordsStartingConsonant(input);
    }
}
